package com.datonicgroup.narrate.app.dataprovider.sync;

import com.datonicgroup.narrate.app.dataprovider.Settings;
import com.datonicgroup.narrate.app.models.SyncService;

/**
 * Created by timothymiko on 10/27/14.
 */
public class JrnlSyncService extends SimpleAbsSyncService {

    JrnlSyncService(JrnlFileSync fs) {
        super(SyncService.Jrnl, fs);
    }

    JrnlSyncService() {
        this(new JrnlFileSync(Settings.getJrnlSyncToken()));
    }

}
