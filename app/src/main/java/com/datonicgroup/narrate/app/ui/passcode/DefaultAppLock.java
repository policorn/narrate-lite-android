package com.datonicgroup.narrate.app.ui.passcode;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;

import com.datonicgroup.narrate.app.BuildConfig;
import com.datonicgroup.narrate.app.ui.dialogs.PasscodeLockTimeDialog;

import java.util.Arrays;
import java.util.Date;

import javax.crypto.Cipher;

import android.security.KeyPairGeneratorSpec;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;

import android.util.Log;

public class DefaultAppLock extends AbstractAppLock {

    static final String TAG = "DefaultAppLock";
    static final String CIPHER_TYPE = "RSA/ECB/PKCS1Padding";
    static final Integer VALIDITY_YEARS = 100;

    private Application currentApp; //Keep a reference to the app that invoked the locker
    private SharedPreferences settings;
    private Date lostFocusDate;

    private KeyStore keyStore;
    private List<String> keyAliases;

    public DefaultAppLock(Application currentApp) {
        super();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(currentApp);
        this.settings = settings;
        this.currentApp = currentApp;

        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
        }
        catch(Exception e) {}
        refreshKeys();
    }

    private void refreshKeys() {
        keyAliases = new ArrayList<>();
        try {
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                keyAliases.add(aliases.nextElement());
            }
        }
        catch(Exception e) {}

    }

    public void enable(){
    	if (android.os.Build.VERSION.SDK_INT < 18)
    		return;

        if( isPasswordLocked() ) {
            currentApp.unregisterActivityLifecycleCallbacks(this);
            currentApp.registerActivityLifecycleCallbacks(this);
        }
    }

    public void disable( ){
    	if (android.os.Build.VERSION.SDK_INT < 18)
    		return;

        currentApp.unregisterActivityLifecycleCallbacks(this);
    }

    public void forcePasswordLock(){
        lostFocusDate = null;
    }

    public boolean verifyPassword( String password ){
    	String storedPassword = "";

        if (settings.contains(BuildConfig.PASSWORD_PREFERENCE_KEY)) {
    		//read the password from the new key
    		storedPassword = settings.getString(BuildConfig.PASSWORD_PREFERENCE_KEY, "");
            storedPassword = decryptString(BuildConfig.PASSWORD_PREFERENCE_KEY, storedPassword);
    	}

        if( password.equalsIgnoreCase(storedPassword) ) {
            lostFocusDate = new Date();
            return true;
        } else {
            return false;
        }
    }

    public boolean setPassword(String password){

        SharedPreferences.Editor editor = settings.edit();

        if(password == null) {
            editor.remove(BuildConfig.PASSWORD_PREFERENCE_KEY);
            editor.apply();
            this.disable();
        } else {
            createNewKeys(BuildConfig.PASSWORD_PREFERENCE_KEY);
            password = encryptString(BuildConfig.PASSWORD_PREFERENCE_KEY, password);
            editor.putString(BuildConfig.PASSWORD_PREFERENCE_KEY, password);
            editor.apply();
            this.enable();
        }

        return true;
    }

    //Check if we need to show the lock screen at startup
    public boolean isPasswordLocked(){

        return settings.contains(BuildConfig.PASSWORD_PREFERENCE_KEY);

    }

    public void createNewKeys(String alias) {
        try {
            // Create new key if needed
            if (!keyStore.containsAlias(alias)) {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, VALIDITY_YEARS);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(currentApp.getApplicationContext())
                        .setAlias(alias)
                        .setSubject(new X500Principal("CN="+BuildConfig.APPLICATION_ID+", O=Android Authority"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
                generator.initialize(spec);

                KeyPair keyPair = generator.generateKeyPair();
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        refreshKeys();
    }

    public void deleteKey(final String alias) {
        try {
            keyStore.deleteEntry(alias);
            refreshKeys();
        } catch (KeyStoreException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    public String encryptString(String alias, String plaintext) {

        String encryptedText = null;

        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();

            Cipher input = Cipher.getInstance(CIPHER_TYPE);
            input.init(Cipher.ENCRYPT_MODE, publicKey);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(
                    outputStream, input);
            cipherOutputStream.write(plaintext.getBytes("UTF-8"));
            cipherOutputStream.close();

            byte [] vals = outputStream.toByteArray();
            encryptedText = (Base64.encodeToString(vals, Base64.DEFAULT));

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return encryptedText;
    }

    public String decryptString(String alias, String encryptedText) {

        String decryptedText = null;

        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);

            Cipher output = Cipher.getInstance(CIPHER_TYPE);
            output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());

            CipherInputStream cipherInputStream = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(encryptedText, Base64.DEFAULT)), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte)nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for(int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i).byteValue();
            }

            String finalText = new String(bytes, 0, bytes.length, "UTF-8");
            decryptedText = finalText;

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        return decryptedText;
    }

    private boolean mustShowUnlockSceen() {

        if( isPasswordLocked() == false)
            return false;

        if( lostFocusDate == null )
            return true; //first startup or when we forced to show the password

        int currentTimeOut = lockTimeOut; //get a reference to the current password timeout and reset it to default
        lockTimeOut = DEFAULT_TIMEOUT;
        Date now = new Date();
        long now_ms = now.getTime();
        long lost_focus_ms = lostFocusDate.getTime();
        int secondsPassed = (int) (now_ms - lost_focus_ms)/(1000);
        secondsPassed = Math.abs(secondsPassed); //Make sure changing the clock on the device to a time in the past doesn't by-pass PIN Lock
        if (secondsPassed >= currentTimeOut) {
            lostFocusDate = null;
            return true;
        }

        return false;
    }

    @Override
    public void onActivityPaused(Activity arg0) {

        if( arg0.getClass() == PasscodeUnlockActivity.class )
            return;

        if( ( this.appLockDisabledActivities != null ) && Arrays.asList(this.appLockDisabledActivities).contains( arg0.getClass().getName() ) )
     	   return;

        lostFocusDate = new Date();

    }

    @Override
    public void onActivityResumed(Activity arg0) {

        if( arg0.getClass() == PasscodeUnlockActivity.class )
            return;

       if(  ( this.appLockDisabledActivities != null ) && Arrays.asList(this.appLockDisabledActivities).contains( arg0.getClass().getName() ) )
    	   return;

        if(mustShowUnlockSceen()) {
            //uhhh ohhh!
            Intent i = new Intent(arg0.getApplicationContext(), PasscodeUnlockActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            arg0.getApplication().startActivity(i);
            return;
        }

    }

    @Override
    public void onActivityCreated(Activity arg0, Bundle arg1) {
        int index = PreferenceManager.getDefaultSharedPreferences(arg0).getInt("passcode_timeout_index", 0);
        if ( index < 0 ) index = 0;

        int val = PasscodeLockTimeDialog.values[index];
        DEFAULT_TIMEOUT = val;
    }

    @Override
    public void onActivityDestroyed(Activity arg0) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity arg0, Bundle arg1) {
    }

    @Override
    public void onActivityStarted(Activity arg0) {
    }

    @Override
    public void onActivityStopped(Activity arg0) {
    }
}
