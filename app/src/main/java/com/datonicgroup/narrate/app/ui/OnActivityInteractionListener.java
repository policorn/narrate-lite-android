package com.datonicgroup.narrate.app.ui;

/**
 * Created by timothymiko on 8/3/14.
 */
public interface OnActivityInteractionListener {

    boolean onBackPressed();

}
