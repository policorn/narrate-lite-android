
# Narrate Lite
This is a fork of [Narrate Android APP](https://github.com/timothymiko/narrate-android),
liberated by Timothy Miko on [May 2016 under Apache License v2.0](https://github.com/timothymiko/narrate-android/commit/3d946bd65b78f8c1b68b4c7932e4f9f19a7f3573#diff-9879d6db96fd29134fc802214163b95a)

This is a privacy-focused version, aiming to completely remove Google, 3rd party stats and any
other communication without explicit user consent

## Roadmap

- Make it compile again (**DONE**)
- Remove Crashlytics log reporting (**DONE**)
- Remove DropBox code (**DONE**)
- Remove Google dependencies (**DONE**)
- Hide/Remove location functionality while porting to MapsForge (**DONE**)
- Replace Google Maps by MapsForge / Open Street Maps (**In progress**)
- Comply with F-Droid publishing requirements (**In progress**)
- Use Android KeyStore to secure application lock secrets (**DONE**)
- Introduce Bullet Journal / Tracker capabilities

## About

<img src="banner.png">

Narrate is a beautiful journal created to give you the ability to record your thoughts,
experiences, and ideas. With the ability to sync your journal across all of your devices, you won't
have ever be without your journal. With a minimalist, material user interface, Narrate has a
stunning user experience that is unmatched in this category.


## Install
If you would like to manually build and install Narrate on your Android device.

To manually build and install Narrate:

1. Create a keystore.properties file in the root directory (see keystory.properties.example).
2. Create a local.properties file in the root directory (see local.properties.example).

## Bugs & Feature Requests
Please use GitLab's issue tracker for all bugs and feature requests. Please search through all of
the issues before creating a new issue.

## Support
Please refer to the wiki for any support-related questions.

## Contribution Guidelines
Public contributions are welcomed in the form of pull requests with a clear description of your
changes and if they relate to any open issues.
